'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var yosay = require('yosay');

var HlupGenerator = yeoman.generators.Base.extend({
  initializing: function () {
    this.pkg = require('../package.json');
  },

  prompting: function () {
    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the wondrous Hlup generator!'
    ));
  },

  writing: {
    app: function () {
      this.dest.mkdir('app');
      this.dest.mkdir('app/source');
      this.dest.mkdir('app/source/css');
      this.dest.mkdir('app/source/fonts');
      this.dest.mkdir('app/source/i');
      this.dest.mkdir('app/source/js');
      this.dest.mkdir('app/source/partials');
      this.dest.mkdir('app/source/partials/_dev');
      this.dest.mkdir('app/source/stylus');

      this.src.copy('_package.json', 'package.json');
      this.src.copy('_bower.json', 'bower.json');
    },

    projectfiles: function () {
      // this.src.copy('editorconfig', '.editorconfig');
      // this.src.copy('jshintrc', '.jshintrc');
      this.src.copy('.gitattributes', '.gitattributes');
      this.src.copy('.gitignore', '.gitignore');
      this.src.copy('_gulpfile.js', 'gulpfile.js');
      this.src.copy('_index.jade', 'app/source/index.jade');

      this.src.copy('partials/_dev/_dev.jade', 'app/source/partials/_dev/dev.jade');
      this.src.copy('partials/_content.jade', 'app/source/partials/content.jade');
      this.src.copy('partials/_footer.jade', 'app/source/partials/footer.jade');
      this.src.copy('partials/_head.jade', 'app/source/partials/head.jade');
      this.src.copy('partials/_header.jade', 'app/source/partials/header.jade');

      this.src.copy('stylus/__content.styl', 'app/source/stylus/_content.styl');
      this.src.copy('stylus/__footer.styl', 'app/source/stylus/_footer.styl');
      this.src.copy('stylus/__header.styl', 'app/source/stylus/_header.styl');
      this.src.copy('stylus/_simple-flex-grid.styl', 'app/source/stylus/simple-flex-grid.styl');
      this.src.copy('stylus/_main.styl', 'app/source/stylus/main.styl');
    }
  },

  end: function () {
    this.installDependencies();
  }
});

module.exports = HlupGenerator;
